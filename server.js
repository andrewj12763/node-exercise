const express = require("express");
const port = 3000;
const axios = require('axios')

const app = express();
app.use(express.json())

const get_all_people = async () => {
    // The poeple api is paginating the so results we are looping
    // through each data set to return the full array.
    let poeple_array = [];
    let running = true;
    let url = "https://swapi.dev/api/people/";
    
    while(running) {
        await axios
            .get(url, {})
            .then((response) => {
                const data = response.data;
                if (data) {
                    poeple_array = [...poeple_array, ...data.results];
                    // Testing if there is a next set of poeple
                    running = !!response.data.next;
                    // Setting next api url to pull next set of poeple
                    url = response.data.next;
                } else {
                    console.error("Error");
                }
            })
            .catch((error) => {
                console.error(error)
            })
    }

    return poeple_array;
}

const get_sorted_poeple = async (req, res) => {
    const people = await get_all_people();
    const sortBy = req.query.sortBy;
    // If a sortBy is set in the query, then use it to sort 
    const sorted_array = (sortBy && ["name", "height", "mass"].includes(sortBy)) && 
        people.sort((a,b) => {
            let sort_a = a[sortBy] !== "unknown" ? a[sortBy] : "0";
            let sort_b = b[sortBy] !== "unknown" ? b[sortBy] : "0";
            // Check if the sort is a number, and format it to sort correctly
            if(parseInt(sort_a.replace(/,/g, '')) || parseInt(sort_b.replace(/,/g, ''))) {
                sort_a = parseInt(sort_a.replace(/,/g, ''));
                sort_b = parseInt(sort_b.replace(/,/g, ''));
            }
           return (sort_a > sort_b) ? 1 : ((sort_b > sort_a) ? -1 : 0)});

    res.json(sorted_array ? sorted_array : people);
}

const get_all_planets = async () => {
    // The planets api is paginating the so results we are looping
    // through each data set to return the full array.
    let planets_array = [];
    let running = true;
    let url = "https://swapi.dev/api/planets/";
    
    while(running) {
        await axios
            .get(url, {})
            .then((response) => {
                const data = response.data;
                if (data) {
                    planets_array = [...planets_array, ...data.results];
                    // Testing if there is a next set of planets
                    running = !!response.data.next;
                    // Setting next api url to pull next set of planets
                    url = response.data.next;
                } else {
                    console.error("Error");
                }
            })
            .catch((error) => {
                console.error(error)
            })
    }

    return planets_array;
}

const get_planets = async (req, res) => {
    const planets = await get_all_planets();
    const people = await get_all_people();

    // Map through the planets and replace the residents URLs the the their full name 
    const new_planets = planets.map(planet => {
        return {...planet, residents: planet.residents.map(resident => {
            const name = people.find(person => person.url === resident).name;
            return name ? name : resident;
        })}
    });
    res.json(new_planets);
}


app.use("/people", get_sorted_poeple);
app.use("/planets", get_planets);

app.listen(port, function(){
    console.log('Listening on port: ', port);
})